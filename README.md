# David Tuan's README

**David Tuan | Chief of Staff, Product**

Thanks for taking the time to peruse this! My name is David Tuan and I am a member of GitLab's Product Division, where I specifically operate as the Chief of Staff to GitLab's Chief Product Officer. This README document is intended to provide context on who I am, what makes me tick, how I interact and operate with others, and ultimately help folks understand how to best connect and work with me effectively.

## About Me
* I was born and raised in Honolulu, HI. While I've lived more years outside the state of Hawaii at this point, a large piece of my identity was formed through my upbringing in the 50th state and it's unique history, culture, and values. To a degree Hawaii will always still be "home".    
* I am the youngest of four kids, and both of my parents were educators in some fashion. They instilled a strong value on education and sacrificed to find a means to put all four of us through [Punahou School](https://en.wikipedia.org/wiki/Punahou_School).
* My first job was delivering newspapers...lots of newspapers. At one point I had 4 paper routes which amounted to 200+ daily customers and 350+ Sunday customers. The news cycle never rests so delivering newspapers was a 7 days a week, 365 days a year endeavor. If we have a coffee chat, ask about the free trip to France I won as a newspaper carrier...
* I earned my undergraduate degree in Electrical Engineering from Brown University. While I matriculated with every intent to be a pre-med History major, the [Open Curriculum](https://en.wikipedia.org/wiki/Open_Curriculum_(Brown_University)) was a little too open and flexible for me, and I pivoted to concentrating in Engineering which provided more structure/scaffolding. The pre-med aspirations were quickly extinguished after a rough encounter with Organic Chemistry.
* During my undergrad years, I spent 2 summers in upstate New York working at [Corning Inc.](https://en.wikipedia.org/wiki/Corning_Inc.)'s Sullivan Park research center. Those summers were invaluable in helping me understand I lacked enough passion for any one science/engineering field to build a career and invest a Ph.D. into. Those 2 summers also represented my longest continuous stays in a hotel, at 8 weeks and 10 weeks respectively.
* My first job out of undergrad was at [Lucent Technologies](https://en.wikipedia.org/wiki/Lucent) working on automated manufacturing tests for long-haul and ultra long-haul DWDM transport systems. If I inadvertently slip optics and optical networking lingo into conversations, now you know why. 
* I crammed 1+ years of coursework for a Masters degree from Columbia University in Telecommunications Engineering into a 4+ years timeframe - net takeaway that simultaneous part-time school and full-time work can be challenging. Fun fact is that I earned my degree completely asynchronously through distance learning, where the early days involved overnight mailing of VHS cassettes of lectures back-and-forth!
* I learned the value of wearing free SWAG, as I literally can attribute my ~22 year run at Spirent to wearing a vendor's T-Shirt while in line at the Honolulu DMV. Given most of my professional life has been spent in the network test and measurement space, if I inject lingo like [goodput](https://en.wikipedia.org/wiki/Goodput) or start drawing analogies around 64-byte packets into a discussion, now you know why...


## Professional Highlights
I'm on a continuous journey to "figure out what I want to be when I grow up"!  You can check out my [LinkedIn](https://www.linkedin.com/in/dtuan/) profile for more details, but here is a TL;DR summary of my career arc:
* Started my career as a software development engineer in test, but quickly pivoted to more customer facing roles in Professional Services as I wanted to be closer to the customer context
* Had the opportunity to lead and build out Professional Services globally across strategy, offerings, and delivery capabilities
* Pivoted from Services to Product - ran Product Management, Engineering, Quality, and Support for lab & test automation software products
* Shifted focus to leading Technical Product Management for a portfolio of product lines and entire lifecycle of product planning, development, and delivery
* Revectored to lead greenfield initiative that spanned incubation, development, and launch of a cloud-native next-gen service assurance offering
* Immediately prior to my current Chief of Staff role, had the amazing privilege and opportunity to contribute to GitLab as part of the [Office of the CEO](https://handbook.gitlab.com/handbook/ceo/office-of-the-ceo/)

## What's It Like Working With Me?
I thought the best way of providing insight into what it's like working with me, or at least what my perception of my working style is, was through unpacking a series of phrases that have been impactful to me that I try to incorporate into how I operate. There is an origin story behind 'how' and 'why' each one of these came to be impactful that is too long to detail out here (but good fodder for a coffee chat!), so I've focused on the portions that influence how I think and work:

### My Working Style - Framed via Top 8 Impactful Phrases 
1. **"If not you, then who?"**
    * Rather than assuming responsibility or accountability exists elsewhere, I'll tend to drive action in areas until I have found a clear or better owner.  This can result in me asking a lot of "why" and "who" questions, as well as potentially stepping into other people's lanes inadvertently. I appreciate GitLab's value on Collaboration and having "[short toes](https://handbook.gitlab.com/handbook/values/#short-toes)" in particular.
1. **"If you want something to improve, measure it. If you want something to improve even more, report on it."**
    * Data tells a story, and I default to trusting that teams will make the right decision the more complete their access and understanding of data is. Having the right measures/metrics act as a forcing function for team members to all "row in the same direction" towards an objective. I also believe there is a boost in engagement, accountability, and ownership within a team when their workstream metrics are published and transparent within and across teams.
1. **"You can't hit what you can't see."** (baseball metaphor)
    * You could recast this as "You can't solve a problem you don't know about"...which manifests itself in me driving to get as much information (both positive and negative, both qualitative and quantitative) out into the open as possible when working through a task or challenge.
1. **"Put yourself in your customer's shoes."**
    * Empathy and the ability to understand a situation or challenge from a customer or another internal function's perspective is a priority for me, as it helps me (a) tune my communication to be more effective with both external and internal customers, as well as (b) help ensure we are solving for the right outcomes.
1. **"Say what you are going to do, do what you said, and manage expecations when your plan changes"**
    * This is still a growth area, but I aim to be proactive in communicating my deliverables and output bounded by dates. I view my outputs as commitments and team members as internal customers on the receiving end of those commitments. Knowing that it is not a matter of "if" but more "when" plans change, my goal is to be as proactive as possible on communicating changes and resetting expectations in real-time.
1. **"I never remember the questions I got right, only the ones I got wrong."**
    * Failure is a great teacher, and I personally learn more from where and when I fail than where I succeed. I'm a big believer in failing fast and forward at a team or organizational scope, and am growing in my comfort of "failing early, failing often, failing forward" as an individual. I appreciate GitLab's value on [Iteration](https://handbook.gitlab.com/handbook/values/#iteration) in this respect.
1. **"Perfection is not attainable, but if we chase perfection we can catch excellence."**
    * Those are Vince Lombardi's words, and they speak to me around the never ending quest for continous improvement. By nature, I look at how we can improve from current state, and the negative consequence of this is that I am not good at celebrating wins/victories - I'll naturally gravitate towards the never ending backlog of improvements to tackle next.  
1. **"I'm not afraid of building a new feature or widget, but I'm terrified about it's support and maintenance."**
    * I tend to take an end-to-end systems view of things, and evaluate prospective solution options with an eye towards scale and survivability. This is consistent around technical matters, as well as non-technical items that span people, process, team dynamics, and team behaviors.

### Communicating With Me
* **Synchronous Meetings:** My calendar is up-to-date and the SSoT (Single Source of Truth) regarding my availability. I prefer to err on the side of over-communicating, so always happy to jump on a synchronous Zoom call if it drives efficiency on the overall outcome. I've worked with non-US based teams and customers extensively in past work experiences, and with enough advance notice will do my best to support hours that are convenient for others. Full disclosure that I am more of a night owl than a morning bird, and my efficacy in the morning directly correlates with caffeine intake.  On that note, I'm always open to coffee chats!
* **Slack:** I monitor Slack throughout the day, with the exception being when I am in a synchronous meeting where I'm still honing my Live Doc meeting skills. 
* **GitLab Issues:** I'm still growing this muscle, and I definitely have the most latency here in terms of my response times. If I am dropping the ball on getting back to you within an issue, please DM me via Slack!  

## Personality Tests
* **Myers-Briggs Type Indicator**
    * **Past:** back when I first took the MBTI during college, I scored **ESTJ** very strongly
        * [ESTJ - The Director](https://www.verywellmind.com/estj-extraverted-sensing-thinking-judging-2795985) - *"Assertive and rule-oriented, they have high principles and a tendency to take charge."* 
    * **Present:** **ESFJ** according to my latest results, I am very slightly ***F*** versus ***T***, and very slightly ***J*** versus ***P*** 
        * [ESFJ - The Caregiver](https://www.verywellmind.com/esfj-extraverted-sensing-feeling-judging-2795983) - *"Soft-hearted and outgoing, they tend to believe the best about other people."*
            * [16Personalities](https://www.16personalities.com) Results
                * **Personality Type:** [Consul (ESFJ-A)](https://www.16personalities.com/esfj-personality)
                    * Extraverted - 70% | Observant - 63% | Feeling - 52% | Judging - 51% | Assertive - 78% 
                * **Role**: [Sentinel](https://www.16personalities.com/articles/roles-sentinels)
                * **Strategy**: [People Mastery](https://www.16personalities.com/articles/strategies-people-mastery)
* **Enneagram**
    * I've taken the Enneagram many times, but can never recall what type I scored out in terms of primary and wings.  Will come back and post results the next time I take it!

## Life Outside of GitLab
* **Family:** With 2 teenage boys, my wife and I spend a lot of our personal time supporting their aactivities and interests. On any given day that could mean:
    * Parental Uber-ing to a sports activity
    * Camping out at an outdoor sports venue: baseball, American football, cross-country, track
    * Camping out at an indoor sports venue: fencing (foil), basketball
* **Friends:** I've been making a concerted effort since the pandemic to be more intentional with spending quality F2F time with friends. That could range from impromptu tailgates in the parking lot during a kid's practice, to grabbing a casual meal+drinks, to planning a weekend adventure somewhere. 
* **Fitness:** I try and keep reasonably fit, which means I'm (hopefully) squeezing in 4-5 workouts at the gym weekly - always in the evenings, a habit that started when the kids were much younger where their bedtime marked the start of my "personal time" window
* **Sports:** I love playing, watching, talking, debating, and arguing about sports. While I major in team sports as a fan, I do minor in individual sports like tennis, and I have a soft spot for anything that shows up on [ESPN8 The Ocho](https://en.wikipedia.org/wiki/ESPN8_The_Ocho). YouTubeTV and their "Key Plays" feature for recorded games has been life changing.
    * IRL sports I play: softball, basketball, and more recently pickleball
    * Fantasy Sports: baseball, football
* **Travel+Food:** I enjoy traveling as a family unit within the US as well as outside the US. Regardless of where we travel, we always prioritize food adventures alongside, and sometimes over, real adventures!
